<?php

//[SECTION] Repitition Control Structures

//Repitition control structures or "loops" are used to execute code multiple times

//While Loop

//A while loop takes a single condition. If as long as the condition evaluates true, the code inside the block will run.

function whileloop(){
	$count = 5;

	while($count !== 0){
		echo $count . '</br>';
		$count--;
	}
}

//Do-While Loop

//A do-while loop works similarly to a while loop,unlike a while loop, do-while loops quarantee that the code block will be executed at least once

function doWhileLoop(){
	$count = 10;

	do{
		echo $count . '</br>';
		$count--;
	}while($count > 0);
}


//For Loop
/*
	A for loop is a more flrxible kind of loop consisting of three parts:
	-The initial value that will track the progression of the loop

*/

/*	function forLoop(){
		for($count = 0 ); 
	}


function modifiedForLoop(){
	for($count = 0; $count <= 20; $count++){

		if($count % 2 == 0){
			continue;
		}

		echo $count . '</br>';

		if($count > 10){
			break;
		}
	}
}*/

//[SECTION] Array Manipulation

$studentNumbers = array('1923', '1924', '1925', '1926');
//$studentNumbers = ['1923', '1924', '1925', '1926'];

//Simple Array
$grades = [98.5, 94.3, 89.2, 90.1];


//Associative Arrays
$gradePeriods = [
'firstGrading' => 98.5,
'secondGrading' => 94.3,
'thirdGrading' => 89.2,
'fourthGrading' => 90.1
];

//Multi-dimensional Arrays
$heroes = [
	['Iron Man', 'Thor', 'Hulk'],
	['Wolverine', 'Cyclops', 'Jean Grey'],
	['Batman', 'Superman', 'Wonder Woman']
];


//Multi-dimensional Associative Arrays
$powers = [
	'regular' => ['Repulsor Blast', 'Rocket Punch'],
	'signature' => ['Unibeam']
];

$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

//Searching arrays:
function searchBrand($brand, $brandArr){
	if(in_array($brand, $brandArr)){
		return "$brand is in the array.";
	}else{
		return "$brand is NOT in the array.";
	}
}