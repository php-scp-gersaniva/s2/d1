<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2: Repetition Control Structures and Array Manipulation</title>
</head>
<body>

	<h1>Repetition Control Structures</h1>
	<h2>While Loop</h2>
	<?php whileloop(); ?>

	<h2>Do-While Loop</h2>
	<?php doWhileloop(); ?>


	<h1>Array Manipulation</h1>

	<h2>Types of Arrays:</h2>

	<h3>Simple Array</h3>
	<ul>
		<?php foreach($grades as $grade){ ?>
			<li><?= $grade; ?></li>
		<?php } ?>
	</ul>


	<h3>Associative Array</h3>
	<ul>
		<?php foreach($gradePeriods as $period => $grade) { ?>
			<li>Grade in <?= $period . "is" . $grade; ?></li>
		<?php } ?>
	</ul>


//Inner loop finishes first
	<h3>Multi-dimensional Arrays</h3>
	<ul>
		<?php
		foreach($heroes as $team){
			foreach($team as $member){
			?>
			<li><?= $member; ?></li>
		<?php }
		}
		?>
	</ul>


	<h3>Multi-dimensional Associative Arrays</h3>
	<ul>
		<?php 
		foreach($powers as $label => $powerGroup){
			foreach($powerGroup as $power){
				?>
					<li><?= "$label: $power"; ?></li>
			<?php }
		}
		?>
	</ul>

//push to add an array at the end of the current arrays

	<h1>Add to Array</h1>
	<?php array_push($computerBrands, 'Apple'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_unshift($computerBrands, 'Dell'); ?>
	<pre><?php print_r($computerBrands); ?></pre>


	<h3>Remove from Array</h3>
	<?php array_pop($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_shift($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>
	
	<h3>Count</h3>
	<p></p>


	<h3>In Array</h3>
	<p><?= searchBrand('HP', $computerBrands); ?></p>

</body>
</html>
